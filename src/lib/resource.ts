import { getBuildings } from 'lib/grid'
import { get, writable } from 'svelte/store'
import { onMount } from 'svelte'

export const resources = writable(
    {
        elerium: { value: 0, production: 0, max: 0 },
        graphite: { value: 0, production: 0, max: 0 },
        metal: { value: 0, production: 0, max: 0 },
        paste: { value: 0, production: 0, max: 0 },
    },
    set => {
        onMount(() => {
            set({
                elerium: {
                    value: 0,
                    production: getProduction('elerium'),
                    max: getStorage('elerium'),
                },
                graphite: {
                    value: 0,
                    production: getProduction('graphite'),
                    max: getStorage('graphite'),
                },
                metal: {
                    value: 0,
                    production: getProduction('metal'),
                    max: getStorage('metal'),
                },
                paste: {
                    value: 0,
                    production: getProduction('paste'),
                    max: getStorage('paste'),
                },
            })
        })
    }
)

export const getProduction = (resourceName: string) => {
    return getBuildings().reduce((production, building) => {
        if (
            !building.isProduction() ||
            building.resourceName !== resourceName
        ) {
            // Bail on building that aren't production or of the given resource
            return production
        }
        return production + building.getProduction()
    }, 100) // Base production of 100
}

export const getStorage = (resourceName: string) => {
    return getBuildings().reduce((max, building) => {
        if (!building.isStorage() || building.resourceName !== resourceName) {
            // Bail on building that aren't storage or of the given resource
            return max
        }
        return max + building.getCapacity()
    }, 1000) // Base storage of 1000
}

export const updateResource = (): void => {
    setInterval(() => {
        const resourcesData = get(resources)
        const array = ['elerium', 'graphite', 'metal', 'paste']
        array.forEach(resourceName => {
            const newValue =
                resourcesData[resourceName].value +
                resourcesData[resourceName].production
            resourcesData[resourceName].value =
                newValue > resourcesData[resourceName].max
                    ? resourcesData[resourceName].max
                    : newValue
            resources.set(resourcesData)
        })
    }, 1000)
}
