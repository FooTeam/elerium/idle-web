import DistrictField from 'models/DistrictField'
import Natural from 'models/Natural'
import DistrictCouncil from 'models/Buildings/DistrictCouncil'
import { DistrictFieldGradientType } from 'models/DistrictFieldContentInterface'
import { localWritable } from 'lib/localStorage'
import type Building from 'models/Building'
import { get } from 'svelte/store'

const randomNatural = (): Natural => {
    if (Math.random() > 0.3) return null

    const naturals = [
        {
            name: 'GraphiteDeposit',
            color: {
                type: DistrictFieldGradientType.linGrad,
                colors: [
                    { offset: 0.4, color: '#44403C' },
                    { offset: 0.6, color: '#292524' },
                    { offset: 1, color: '#B45309' },
                ],
            },
        },
        {
            name: 'MetalDeposit',
            color: {
                type: DistrictFieldGradientType.linGrad,
                colors: [
                    { offset: 0, color: '#D1D5DB' },
                    { offset: 0.4, color: '#9CA3AF' },
                    { offset: 1, color: '#D1D5DB' },
                ],
            },
        },
        {
            name: 'MeteoriteImpact',
            color: {
                type: DistrictFieldGradientType.radGrad,
                colors: [
                    { offset: 0, color: '#292524' },
                    { offset: 0.4, color: '#57534E' },
                    { offset: 0.6, color: '#78716C' },
                    { offset: 0.6, color: '#A8A29E' },
                    { offset: 0.7, color: '#14532D' },
                    { offset: 1, color: '#4D7C0F' },
                ],
            },
        },
        {
            name: 'FertileSoil',
            color: {
                type: DistrictFieldGradientType.linGrad,
                colors: Array(20)
                    .fill(null)
                    .map((v, index) => {
                        if (index % 2 === 0) {
                            return {
                                offset: (1 / 20) * index,
                                color: '#713F12',
                            }
                        }
                        return { offset: (1 / 20) * index, color: '#4D7C0F' }
                    }),
            },
        },
    ]

    const natural = naturals[Math.floor(Math.random() * naturals.length)]

    return new Natural(natural.name, natural.color)
}

export const generateCity = (): object => {
    console.log('generated ONCE')
    const grid: object = {}

    for (let r = 0; r < 11; r++) {
        grid[r] = {}
        for (let q = 0; q < 15; q++) {
            grid[r][q] = new DistrictField(randomNatural())
        }
    }

    grid[5][7] = new DistrictField(new DistrictCouncil(1))

    return grid
}

export const grid = localWritable('grid', generateCity)

export const getBuildings = (): Building[] => {
    const gridValue = get(grid) as Object
    if (gridValue === undefined) return []

    return Object.values(gridValue).reduce((accumulator: any[], line) => {
        const lineBuildings = Object.values(line).reduce(
            (accumulator: any[], field: DistrictField) => {
                return field.isBuilding()
                    ? [...accumulator, field.content]
                    : accumulator
            },
            []
        )
        return accumulator.concat(lineBuildings)
    }, [])
}
