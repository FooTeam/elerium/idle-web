import type Layout from 'lib/map/Layout'

export enum Directions {
    E,
    SE,
    SW,
    W,
    NW,
    NE,
}

type Direction = Directions
type Cuboid = { q: number; r: number; s: number }
type Axial = { q: number; r: number }
type Coordinates = Cuboid | Axial

export default class Hex {
    readonly q
    readonly r
    readonly s
    readonly offset

    static defaultFillColor: string = '#ddd'
    static defaultStokeColor: string = '#aaa'

    /**
     * All six hexagons directions
     */
    private static directions: Array<Hex> = [
        new Hex({ q: 1, r: 0 }),
        new Hex({ q: 0, r: 1 }),
        new Hex({ q: -1, r: 1 }),
        new Hex({ q: -1, r: 0 }),
        new Hex({ q: 0, r: -1 }),
        new Hex({ q: 1, r: -1 }),
    ]

    constructor(coordinates: Coordinates) {
        const { q, r, s } = this.axialToCuboid(coordinates)
        this.q = q
        this.r = r
        this.s = s
        this.offset = Math.floor(r / 2)
        Object.freeze(this)
    }

    /**
     * Convert Axial to Cuboid coordinates
     */
    private axialToCuboid(coordinates: Axial): Cuboid {
        const { q, r } = coordinates

        return { q, r, s: -q - r }
    }

    /**
     * Add the coordinates of an Hex to the current one
     */
    add(toAdd: Hex): Hex {
        const { q, r, s } = this

        return new Hex({
            q: q + toAdd.q,
            r: r + toAdd.r,
            s: s + toAdd.s,
        })
    }

    /**
     * Return a new Hex representing the neighbor situated in a given direction
     */
    neighbor(direction: Direction): Hex {
        return this.add(Hex.directions[direction])
    }

    /**
     * Turns fractional hex coordinates into the nearest integer ones
     */
    static round(hex: Hex): Hex {
        let q: number = Math.round(hex.q)
        let r: number = Math.round(hex.r)
        let s: number = Math.round(hex.s)

        const qDiff: number = Math.abs(q - hex.q)
        const rDiff: number = Math.abs(r - hex.r)
        const sDiff: number = Math.abs(s - hex.s)

        if (qDiff > rDiff && qDiff > sDiff) {
            q = -r - s
        } else {
            if (rDiff > sDiff) {
                r = -q - s
            } else {
                s = -q - r
            }
        }

        return new Hex({ q, r, s })
    }

    /**
     * Subtracts the coordinates of two given Hex
     */
    public static subtract(a: Hex, b: Hex): Hex {
        return new Hex({
            q: a.q - b.q,
            r: a.r - b.r,
            s: a.s - b.s,
        })
    }

    /**
     * Calculates the length of the line between two Hex in Hex
     */
    public static len(hex: Hex): number {
        return (Math.abs(hex.q) + Math.abs(hex.r) + Math.abs(hex.s)) / 2
    }

    /**
     * Calculates the distance between two Hex in a Hex grid in Hex
     */
    public static distance(a: Hex, b: Hex): number {
        return Hex.len(Hex.subtract(a, b))
    }

    /**
     * Draw the Hex shape and its content
     */
    draw(
        ctx: CanvasRenderingContext2D,
        layout: Layout,
        props: { name: string; color: string | CanvasGradient } = {
            name: '',
            color: Hex.defaultFillColor,
        }
    ) {
        this.drawShape(ctx, layout, props.color)
        this.drawText(ctx, layout, props.name)
    }

    /**
     * Draw the Hex shape
     */
    private drawShape(
        ctx: CanvasRenderingContext2D,
        layout: Layout,
        color: string | CanvasGradient
    ) {
        ctx.fillStyle = color
        ctx.strokeStyle = Hex.defaultStokeColor

        ctx.beginPath()
        for (const point of layout.hexCorners(this)) {
            ctx.lineTo(point.x, point.y)
        }
        ctx.stroke()
        ctx.fill()
    }

    /**
     * Draw a centered text in the Hex
     */
    private drawText(
        ctx: CanvasRenderingContext2D,
        layout: Layout,
        text: string
    ) {
        const center = layout.hexToPixel(this)
        ctx.fillStyle = '#fff'
        ctx.font = '16px sans-serif'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillText(text, center.x, center.y)
    }
}
