import type Orientation from './Orientation'
import Hex from 'lib/map/Hex'
import Point from 'lib/map/Point'

export default class Layout {
    constructor(
        public orientation: Orientation,
        public size: Point,
        public origin: Point
    ) {}

    /**
     * Convert an Hex coordinates to pixel coordinates
     * The returned Point corresponds to the Hex center
     */
    hexToPixel(hex: Hex): Point {
        const f = this.orientation.forward
        const { q, r } = hex
        const x = (f[0] * q + f[1] * r) * this.size.x
        const y = (f[2] * q + f[3] * r) * this.size.y

        return new Point(x + this.origin.x, y + this.origin.y)
    }

    /**
     * Convert pixel coordinates to Hex grid coordinates
     * Returns the corresponding Hex
     */
    pixelToHex(point: Point): Hex {
        const b = this.orientation.inverse
        const p = new Point(
            (point.x - this.origin.x) / this.size.x,
            (point.y - this.origin.y) / this.size.y
        )
        const q = b[0] * p.x + b[1] * p.y
        const r = b[2] * p.x + b[3] * p.y

        return Hex.round(new Hex({ q, r }))
    }

    /**
     * Get all six Hex corners as Points
     */
    hexCorners(hex: Hex): Array<Point> {
        let corners: Array<Point> = []
        const center: Point = this.hexToPixel(hex)

        for (let theta = 0; theta <= Math.PI * 2; theta += Math.PI / 3) {
            corners.push(
                new Point(
                    center.x + this.size.x * Math.sin(theta),
                    center.y + this.size.y * Math.cos(theta)
                )
            )
        }

        return corners
    }
}
