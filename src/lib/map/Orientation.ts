type Matrix = [number, number, number, number]

export default class Orientation {
    private constructor(public forward: Matrix, public inverse: Matrix) {}

    /**
     * Pointy matrices to convert a Hex to pixel and vice versa
     * https://www.redblobgames.com/grids/hexagons/implementation.html#layout
     */
    static pointy = new Orientation(
        [Math.sqrt(3.0), Math.sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0],
        [Math.sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0]
    )
}
