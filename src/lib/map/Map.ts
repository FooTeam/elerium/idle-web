import Hex from 'lib/map/Hex'
import Layout from 'lib/map/Layout'
import Point from 'lib/map/Point'
import Orientation from 'lib/map/Orientation'
import { get } from 'svelte/store'
import {
    DistrictFieldColor,
    DistrictFieldGradientColorStop,
    DistrictFieldGradientType,
} from 'models/DistrictFieldContentInterface'
import { grid as gridStore } from 'lib/grid'
import type DistrictField from 'models/DistrictField'

interface MapOptions {
    canvas: HTMLCanvasElement
    canvasWidth: number
    columns?: number
    rows?: number
}

export default class Map {
    canvas: HTMLCanvasElement
    ctx: CanvasRenderingContext2D

    canvasWidth: number
    canvasHeight: number
    columns: number
    rows: number

    edgeSize: number
    hovered: Hex

    layout: Layout

    private map: Array<Hex> = []

    constructor(options: MapOptions) {
        this.canvas = options.canvas
        this.canvasWidth = options.canvasWidth
        this.columns = options.columns || 15
        this.rows = options.rows || 11

        this.calculateEdgeSize()
        this.calculateCanvasHeight()

        this.storeMap()

        this.getContext()

        this.drawMap()
    }

    /**
     * Calculate the distance between the center and any corner of on Hex depending on the grid to create
     */
    private calculateEdgeSize() {
        this.edgeSize =
            this.canvasWidth / (this.columns * Math.sqrt(3) + Math.sqrt(3) / 2)
    }

    /**
     * Calculate the Canvas height depending on the calculated Hex size
     */
    private calculateCanvasHeight() {
        this.canvasHeight =
            this.rows * ((2 * this.edgeSize * 3) / 4) + this.edgeSize / 2
    }

    /**
     * Store all the Hex in a rectangular shaped 2D array
     */
    private storeMap() {
        const size = new Point(this.edgeSize, this.edgeSize)
        const origin = new Point(Orientation.pointy.forward[1] * size.x, size.y)
        this.layout = new Layout(Orientation.pointy, size, origin)

        for (let r = 0; r < this.rows; r++) {
            const offset = Math.floor(r / 2)
            for (let q = -offset; q < this.columns - offset; q++) {
                const hex = new Hex({ q, r })
                this.map.push(hex)
            }
        }
    }

    /**
     * Get the canvas 2D context
     */
    private getContext(): CanvasRenderingContext2D {
        this.canvas.width = this.canvasWidth
        this.canvas.height = this.canvasHeight
        this.ctx = this.canvas.getContext('2d', { alpha: false })

        return this.ctx
    }

    /**
     * Draw each Hex of the map on the canvas context
     */
    drawMap() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.ctx.save()

        const grid = get(gridStore)

        for (const hex of this.map) {
            const field: DistrictField = grid?.[hex.r]?.[hex.q + hex.offset]

            hex.draw(this.ctx, this.layout, {
                name: field.getShortName(),
                color:
                    this.createColor(field.content?.color, hex) ||
                    Hex.defaultFillColor,
            })
        }

        this.ctx.restore()
    }

    /**
     * Create the color from the DistrictField configuration
     */
    private createColor(
        color: DistrictFieldColor,
        hex: Hex
    ): string | CanvasGradient {
        if (!color) return

        if (typeof color === 'string') {
            return color
        }

        const center = this.layout.hexToPixel(hex)
        const halfWidth = Orientation.pointy.forward[1] * this.edgeSize
        if (color.type === DistrictFieldGradientType.linGrad) {
            return this.buildLinearGradient(color.colors, center, halfWidth)
        }

        if (color.type === DistrictFieldGradientType.radGrad) {
            return this.buildRadialGradient(color.colors, center, halfWidth)
        }
    }

    /**
     * Build a linear gradient from the Hex top left to the Hex bottom right
     */
    private buildLinearGradient(
        colorStops: Array<DistrictFieldGradientColorStop>,
        center: Point,
        halfWidth: number
    ): CanvasGradient {
        return Map.addColorStops(
            colorStops,
            this.ctx.createLinearGradient(
                center.x - halfWidth,
                center.y - this.edgeSize,
                center.x + halfWidth,
                center.y + this.edgeSize
            )
        )
    }

    /**
     * Build a radial gradient from the Hex center
     */
    private buildRadialGradient(
        colorStops: Array<DistrictFieldGradientColorStop>,
        center: Point,
        halfWidth: number
    ): CanvasGradient {
        return Map.addColorStops(
            colorStops,
            this.ctx.createRadialGradient(
                center.x,
                center.y,
                this.edgeSize / 6,
                center.x,
                center.y,
                halfWidth
            )
        )
    }

    /**
     * Add color stops to gradients following the DistrictField configuration
     */
    private static addColorStops(
        colorStops: Array<DistrictFieldGradientColorStop>,
        gradient: CanvasGradient
    ): CanvasGradient {
        for (const { offset, color } of colorStops) {
            gradient.addColorStop(offset, color)
        }

        return gradient
    }

    /**
     * Mask the hovered Hex with a semi transparent black Hex
     */
    hover(event: MouseEvent) {
        const hex = this.layout.pixelToHex(Map.eventToPoint(event))

        if (this.isHexInMap(hex)) {
            this.drawMap()

            const field = get(gridStore)?.[hex.r]?.[hex.q + hex.offset]
            hex.draw(this.ctx, this.layout, {
                name: field.getShortName(),
                color: 'rgba(0, 0, 0, 0.3)',
            })

            this.hovered = hex
        }
    }

    /**
     * Check if a Hex exists in the map
     */
    isHexInMap(hex: Hex): boolean {
        return this.map.some(h => Hex.distance(h, hex) === 0)
    }

    /**
     * Convert an event to a Point
     */
    private static eventToPoint(event: MouseEvent): Point {
        return new Point(event.offsetX, event.offsetY)
    }
}
