import { get, writable } from 'svelte/store'

const getInitialValue = initialValue =>
    typeof initialValue === 'function' ? initialValue() : initialValue

const getValue = (key: string, initialValue) => {
    // Récupère depuis le localStorage
    const json = localStorage.getItem(key)

    // On utilise la valeur initiale si rien n'est sauvegardé dans le localStorage
    // if (json && json !== 'undefined') {
    //     return JSON.parse(json)
    // }

    return getInitialValue(initialValue)
}

// svelte/writable wrapper avec persistence à travers le localStorage
export const localWritable = (key: string, initialValue: any, start?) => {
    if (typeof localStorage === 'undefined') {
        return writable(getInitialValue(initialValue))
    }

    const value = getValue(key, initialValue)
    const browserWritable = start
        ? writable(value, () => start(localSet))
        : writable(value)

    localStorage.setItem(key, JSON.stringify(value))
    const { subscribe, set } = browserWritable

    // assigne la valeur dans le store et dans le localStorage
    const localSet = value => {
        localStorage.setItem(key, JSON.stringify(value))
        set(value)
    }
    // modifie la valeur dans le store et dans le localStorage
    const localUpdate = callback => {
        localSet(callback(get(browserWritable)))
    }

    return {
        set: localSet,
        update: localUpdate,
        // Expose la fonction `subscribe` du store
        subscribe,
    }
}
