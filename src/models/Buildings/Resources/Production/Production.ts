import Building from 'models/Building'
import { get } from 'svelte/store'
import { resources, getProduction } from 'lib/resource'

export default abstract class Production extends Building {
    resourceName: string

    protected constructor(
        resourceName: string,
        name: string,
        color: string,
        level?: number
    ) {
        super(name, color, level)
        this.resourceName = resourceName
    }

    abstract getProduction(): number

    protected endUpgrade() {
        const oldProduction = this.getProduction()
        super.endUpgrade()
        const resourcesData = get(resources)
        resourcesData[this.resourceName].production +=
            this.getProduction() - oldProduction
        resources.set(resourcesData)
    }
}
