import Production from './Production'

export default class GraphiteMine extends Production {
    constructor(level?: number) {
        super('graphite', 'GraphiteMine', '#44403C', level)
    }

    getNeededGraphite(): number {
        return 100 + this.level * 400
    }

    getNeededMetal(): number {
        return 100 + this.level * 400
    }

    getProduction(): number {
        return this.level * 120
    }

    getConstructionTime(): number {
        return this.level * 5000
    }
}
