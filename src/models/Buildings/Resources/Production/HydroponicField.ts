import Production from './Production'

export default class HydroponicField extends Production {
    constructor(level?: number) {
        super('paste', 'HydroponicField', '#4D7C0F', level)
    }

    getNeededGraphite(): number {
        return 100 + this.level * 400
    }

    getNeededMetal(): number {
        return 100 + this.level * 400
    }

    getProduction(): number {
        return this.level * 120
    }

    getConstructionTime(): number {
        return this.level * 5000
    }
}
