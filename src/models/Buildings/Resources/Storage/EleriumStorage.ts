import Storage from './Storage'

export default class EleriumStorage extends Storage {
    constructor(level?: number) {
        super('elerium', 'EleriumStorage', '#101555', level)
    }

    getCapacity(): number {
        return (this.level || 0) * 1000
    }

    getNeededGraphite(): number {
        return 100 + this.level * 500
    }

    getNeededMetal(): number {
        return 100 + this.level * 500
    }

    getConstructionTime(): number {
        return this.level * 5000
    }
}
