import Building from 'models/Building'
import { get } from 'svelte/store'
import { resources } from 'lib/resource'

export default abstract class Storage extends Building {
    resourceName: string

    protected constructor(
        resourceName: string,
        name: string,
        color: string,
        level?: number
    ) {
        super(name, color, level)
        this.resourceName = resourceName
    }

    abstract getCapacity(): number

    // Update the capacity in the store
    protected endUpgrade() {
        const oldCapacity = this.getCapacity()
        super.endUpgrade()
        const resourcesData = get(resources)
        resourcesData[this.resourceName].max += this.getCapacity() - oldCapacity
        resources.set(resourcesData)
    }
}
