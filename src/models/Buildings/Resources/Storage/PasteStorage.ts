import Storage from './Storage'

export default class PasteStorage extends Storage {
    constructor(level?: number) {
        super('paste', 'PasteStorage', '#4D7C0F', level)
    }

    getCapacity(): number {
        return (this.level || 0) * 1000
    }

    getNeededGraphite(): number {
        return 100 + this.level * 500
    }

    getNeededMetal(): number {
        return 100 + this.level * 500
    }

    getConstructionTime(): number {
        return this.level * 5000
    }
}
