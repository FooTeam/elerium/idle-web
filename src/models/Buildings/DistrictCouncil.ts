import Building from 'models/Building'

export default class DistrictCouncil extends Building {
    constructor(level?: number) {
        super('DistrictCouncil', '#0EA5E9', level)
    }

    getNeededGraphite(): number {
        return 1
    }

    getNeededMetal(): number {
        return 1
    }

    getConstructionTime(): number {
        return this.level * 5000
    }
}
