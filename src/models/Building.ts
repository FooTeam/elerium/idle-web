import type DistrictFieldContentInterface from 'models/DistrictFieldContentInterface'
import type { DistrictFieldColor } from 'models/DistrictFieldContentInterface'
import Timer from '../utils/Timer'
import { grid } from 'lib/grid'
import { get } from 'svelte/store'
import { resources } from 'lib/resource'
import type Storage from 'models/Buildings/Resources/Storage/Storage'
import type Production from 'models/Buildings/Resources/Production/Production'

export default abstract class Building
    implements DistrictFieldContentInterface {
    name: string
    level: number
    construction: Timer

    protected constructor(
        name: string,
        public color: DistrictFieldColor,
        level?: number
    ) {
        this.name = name
        this.level = level || 0
        this.construction = new Timer()
    }

    abstract getNeededMetal(): number
    abstract getNeededGraphite(): number
    abstract getConstructionTime(): number

    protected checkNeededResources(): boolean {
        const { metal, graphite } = get(resources)

        return (
            metal.value >= this.getNeededMetal() &&
            graphite.value >= this.getNeededGraphite()
        )
    }

    protected checkNeededResearches(): true {
        // @TODO : implement
        return true
    }

    protected checkNeeded(): boolean {
        return this.checkNeededResearches() && this.checkNeededResources()
    }

    isProduction(): this is Production {
        // @ts-ignore
        return typeof this.getProduction !== 'undefined'
    }

    isStorage(): this is Storage {
        // @ts-ignore
        return typeof this.getCapacity !== 'undefined'
    }

    isUnderConstruction(): boolean {
        return this.construction.id !== undefined
    }

    consumeResource(): void {
        const resourcesData = get(resources)
        resourcesData.metal.value -= this.getNeededMetal()
        resourcesData.graphite.value -= this.getNeededGraphite()
        resources.set(resourcesData)
    }

    protected endUpgrade(): void {
        this.level++
        grid.update(grid => grid)
    }

    public up(): void {
        if (this.isUnderConstruction()) return
        if (!this.checkNeeded()) return

        this.consumeResource()
        this.construction.start(this.getConstructionTime())
        grid.update(grid => grid)
        setTimeout(this.endUpgrade.bind(this), this.getConstructionTime())
    }
}
