export enum DistrictFieldGradientType {
    linGrad,
    radGrad,
}

type DistrictFieldColorCode = string

export type DistrictFieldGradientColorStop = {
    offset: number
    color: string
}

type DistrictFieldGradient = {
    type: DistrictFieldGradientType
    colors: Array<DistrictFieldGradientColorStop>
}

export type DistrictFieldColor = DistrictFieldColorCode | DistrictFieldGradient

export default interface DistrictFieldContentInterface {
    name: string
    color: DistrictFieldColor
}
