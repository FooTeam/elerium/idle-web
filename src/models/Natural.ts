import type FieldContentInterface from 'models/DistrictFieldContentInterface'
import type { DistrictFieldColor } from 'models/DistrictFieldContentInterface'

export default class Natural implements FieldContentInterface {
    constructor(public name: string, public color: DistrictFieldColor) {}
}
