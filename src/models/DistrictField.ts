import type DistrictFieldContentInterface from 'models/DistrictFieldContentInterface'

export default class DistrictField {
    content: DistrictFieldContentInterface

    constructor(value: DistrictFieldContentInterface) {
        this.content = value
    }

    getShortName(): string {
        return (
            this.content?.name
                .split(/(?=[A-Z])/)
                .reduce((prev, curr) => prev + curr.charAt(0), '') || ''
        )
    }

    isBuilding(): boolean {
        return (
            this.content !== null &&
            Object.prototype.hasOwnProperty.call(this.content, 'level')
        )
    }
}
