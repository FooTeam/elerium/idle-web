import { writable, Writable } from 'svelte/store'

export default class Timer {
    id: number
    _timeLeft: Writable<number> // millisecond

    constructor() {
        this._timeLeft = writable(0)
        this._timeLeft.subscribe(newValue => {
            if (newValue <= 0) {
                clearInterval(this.id)
                this.id = undefined
            }
        })
    }

    public start(duration) {
        this._timeLeft.set(duration)
        this.id = setInterval(this.interval.bind(this), 1000)
    }

    private interval() {
        this._timeLeft.update(old => {
            return old - 1000
        })
    }

    get timeLeft(): Writable<number> {
        return this._timeLeft
    }
}
