const orange = require('tailwindcss/colors').orange
const teal = require('tailwindcss/colors').teal

module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        container: {
            padding: {
                default: '1rem',
                sm: '2rem',
                lg: '4rem',
                xl: '5rem',
            },
        },
        extend: {
            boxShadow: {
                error: '0 0 0 4px rgba(229, 62, 62, 0.5)',
            },
            colors: {
                orange,
                teal,
                midnight: {
                    100: '#7074a1',
                    200: '#626698',
                    300: '#52578e',
                    400: '#414683',
                    500: '#2e3377',
                    600: '#191f69',
                    700: '#02095a',
                    800: '#020644',
                    900: '#020538',
                },
            },
            fontFamily: {
                display: ['Bebas Neue'],
            },
            fontSize: {
                '8xl': '6rem',
            },
            inset: {
                4: '1rem',
            },
            opacity: {
                90: '0.9',
            },
            zIndex: {
                '-10': '-10',
            },
        },
    },
    variants: {},
    plugins: [],
}
